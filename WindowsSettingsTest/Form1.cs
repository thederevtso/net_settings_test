﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsSettingsTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            labelStart.Text = Properties.Settings.Default.WinSettingsHere; ;
        }

        private void buttonRead_Click(object sender, EventArgs e)
        {
            labelNow.Text =  Properties.Settings.Default.WinSettingsHere;
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.WinSettingsHere = textBoxUpdate.Text;
            Properties.Settings.Default.Save();

            labelNow.Text = Properties.Settings.Default.WinSettingsHere;
        }

        private void buttonTestAppSettings_Click(object sender, EventArgs e)
        {
            textBoxTestAppSettings.Text =  ConfigurationSettings.AppSettings["WinSettingsHere"];

            
        }
    }
}
