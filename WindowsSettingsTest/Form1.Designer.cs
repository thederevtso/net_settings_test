﻿namespace WindowsSettingsTest
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.buttonRead = new System.Windows.Forms.Button();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.textBoxUpdate = new System.Windows.Forms.TextBox();
            this.labelStart = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelNow = new System.Windows.Forms.Label();
            this.textBoxTestAppSettings = new System.Windows.Forms.TextBox();
            this.buttonTestAppSettings = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Start";
            // 
            // buttonRead
            // 
            this.buttonRead.Location = new System.Drawing.Point(16, 68);
            this.buttonRead.Name = "buttonRead";
            this.buttonRead.Size = new System.Drawing.Size(75, 23);
            this.buttonRead.TabIndex = 1;
            this.buttonRead.Text = "buttonRead";
            this.buttonRead.UseVisualStyleBackColor = true;
            this.buttonRead.Click += new System.EventHandler(this.buttonRead_Click);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Location = new System.Drawing.Point(309, 68);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(89, 23);
            this.buttonUpdate.TabIndex = 2;
            this.buttonUpdate.Text = "buttonUpdate";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // textBoxUpdate
            // 
            this.textBoxUpdate.Location = new System.Drawing.Point(16, 111);
            this.textBoxUpdate.Name = "textBoxUpdate";
            this.textBoxUpdate.Size = new System.Drawing.Size(526, 20);
            this.textBoxUpdate.TabIndex = 3;
            // 
            // labelStart
            // 
            this.labelStart.AutoSize = true;
            this.labelStart.Location = new System.Drawing.Point(16, 30);
            this.labelStart.Name = "labelStart";
            this.labelStart.Size = new System.Drawing.Size(0, 13);
            this.labelStart.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(306, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Now";
            // 
            // labelNow
            // 
            this.labelNow.AutoSize = true;
            this.labelNow.Location = new System.Drawing.Point(309, 29);
            this.labelNow.Name = "labelNow";
            this.labelNow.Size = new System.Drawing.Size(0, 13);
            this.labelNow.TabIndex = 6;
            // 
            // textBoxTestAppSettings
            // 
            this.textBoxTestAppSettings.Location = new System.Drawing.Point(19, 224);
            this.textBoxTestAppSettings.Name = "textBoxTestAppSettings";
            this.textBoxTestAppSettings.Size = new System.Drawing.Size(523, 20);
            this.textBoxTestAppSettings.TabIndex = 7;
            // 
            // buttonTestAppSettings
            // 
            this.buttonTestAppSettings.Location = new System.Drawing.Point(19, 195);
            this.buttonTestAppSettings.Name = "buttonTestAppSettings";
            this.buttonTestAppSettings.Size = new System.Drawing.Size(181, 23);
            this.buttonTestAppSettings.TabIndex = 8;
            this.buttonTestAppSettings.Text = "buttonTestAppSettings";
            this.buttonTestAppSettings.UseVisualStyleBackColor = true;
            this.buttonTestAppSettings.Click += new System.EventHandler(this.buttonTestAppSettings_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(716, 537);
            this.Controls.Add(this.buttonTestAppSettings);
            this.Controls.Add(this.textBoxTestAppSettings);
            this.Controls.Add(this.labelNow);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelStart);
            this.Controls.Add(this.textBoxUpdate);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.buttonRead);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonRead;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.TextBox textBoxUpdate;
        private System.Windows.Forms.Label labelStart;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelNow;
        private System.Windows.Forms.TextBox textBoxTestAppSettings;
        private System.Windows.Forms.Button buttonTestAppSettings;
    }
}

