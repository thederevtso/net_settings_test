﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SettingsTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string foo;

            foo = SettingsTest.Properties.Settings.Default.IsSettingsString;

            Console.WriteLine(foo);

            Properties.Settings.Default.IsSettingsString = "Another Story";

            foo = SettingsTest.Properties.Settings.Default.IsSettingsString;
            Properties.Settings.Default.Save();

            Console.WriteLine(foo);

            //Properties


            Console.ReadLine();

        }
    }
}
